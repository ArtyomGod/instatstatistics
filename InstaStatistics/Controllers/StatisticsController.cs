﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ConstantLibrary;
using EntityFramework;
using InstaStatistics.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace InstaStatistics.Controllers
{
	[Produces(Constant.Controller.Statistics.Produces)]
	[Route(Constant.Controller.Statistics.Route)]
	[Authorize]
	public class StatisticsController : Controller
	{
		private readonly InstaStatisticsContext _db;
		private readonly IMapper _mapper;
		private readonly UserManager<ApplicationUser> _userManager;

		public StatisticsController(
			InstaStatisticsContext db,
			IMapper mapper,
			UserManager<ApplicationUser> userManager)
		{
			_db = db;
			_mapper = mapper;
			_userManager = userManager;
		}

		[HttpGet]
		public async Task<IActionResult> Get(string accountLogin)
		{
			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				return BadRequest();
			}
			SocialNetworkAccount account = _db.SocialNetworkAccounts.First(a => a.UserId == user.Id && a.Login == accountLogin);
			List<StatisticsDto> metrics = new List<StatisticsDto>
			{
				GetLikesStatistics(account.SocialNetworkAccountId),
				GetViewsStatistics(account.SocialNetworkAccountId),
				GetCommentsStatistics(account.SocialNetworkAccountId)
			};
			
			return Ok(
				new MetricsDto
				{
					Metric = metrics.ToArray()
				}
			);
		}

		StatisticsDto GetLikesStatistics(int accountId)
		{
			List<Tuple<int, DateTime>> userStatisticsLikes = _db.StatisticsSet.Where(statistics => statistics.AccountId == accountId).Select(s => new Tuple<int, DateTime>(s.Likes, s.Date)).ToList();
			return new StatisticsDto
			{
				MetricName = "metric_name_likes",
				DateTimes = userStatisticsLikes.Select(x => x.Item2).ToArray(),
				Values = userStatisticsLikes.Select(x => x.Item1).ToArray(),
			};
		}

		StatisticsDto GetCommentsStatistics(int accountId)
		{
			List<Tuple<int, DateTime>> userStatisticsLikes = _db.StatisticsSet.Where(statistics => statistics.AccountId == accountId).Select(s => new Tuple<int, DateTime>(s.Comments, s.Date)).ToList();
			return new StatisticsDto
			{
				MetricName = "metric_name_comments",
				DateTimes = userStatisticsLikes.Select(x => x.Item2).ToArray(),
				Values = userStatisticsLikes.Select(x => x.Item1).ToArray(),
			};
		}
		StatisticsDto GetViewsStatistics(int accountId)
		{
			List<Tuple<int, DateTime>> userStatisticsLikes = _db.StatisticsSet.Where(statistics => statistics.AccountId == accountId).Select(s => new Tuple<int, DateTime>(s.Views, s.Date)).ToList();
			return new StatisticsDto
			{
				MetricName = "metric_name_photo_views",
				DateTimes = userStatisticsLikes.Select(x => x.Item2).ToArray(),
				Values = userStatisticsLikes.Select(x => x.Item1).ToArray(),
			};
		}

	}
}