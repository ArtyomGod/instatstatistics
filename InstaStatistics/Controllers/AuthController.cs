﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ConstantLibrary;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using InstaStatistics.Dto;
using EntityFramework;
using InstaStatistics.Secure;

namespace InstaStatistics.Controllers
{
	[Route("auth")]
	public class AuthController : Controller
	{
		private readonly IConfiguration _configuration;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;

		public AuthController(
			IConfiguration configuration,
			UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager)
		{
			_configuration = configuration;
			_userManager = userManager;
			_signInManager = signInManager;
		}

		private enum ResultCode
		{
			InvalidLoginAttempt,
			InvalidModel,
			EmailAlreadyConfirmed,
			InvalidPassword,
			EmailIsBusy,
		}

		[HttpPost]
		[Route("sign-in")]
		public async Task<IActionResult> Login([FromBody] ApplicationUserDto model)
		{
			var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

			if (!result.Succeeded)
			{
				return BadRequest(ResultCode.InvalidLoginAttempt);
			}

			var user = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);

			return Ok(JwtTokenHelper.Generate(model.Email, user, _configuration, _userManager));
		}

		[HttpPost]
		[Route("sign-up")]
		public async Task<IActionResult> Register([FromBody] ApplicationUserDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ResultCode.InvalidModel);
			}

			var user = new ApplicationUser
			{
				UserName = model.Username ?? model.Email,
				Email = model.Email
			};

			{
				IdentityResult result = await _userManager.CreateAsync(user, model.Password);
				if (!result.Succeeded)
				{
					return BadRequest(result.Errors);
				}
			}

			{
				IdentityResult result = await _userManager.AddToRoleAsync(user, Constant.Role.CommonUser);
				if (!result.Succeeded)
				{
					return BadRequest(result.Errors);
				}
			}

			await _signInManager.SignInAsync(user, false);
			
			var confirmationTokenTask = _userManager.GenerateEmailConfirmationTokenAsync(user);
			confirmationTokenTask.Wait();
			
			return Ok(JwtTokenHelper.Generate(model.Email, user, _configuration, _userManager));
		}
	}
}
