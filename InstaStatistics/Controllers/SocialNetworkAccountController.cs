﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ConstantLibrary;
using EntityFramework;
using InstaStatistics.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace InstaStatistics.Controllers
{
	[Produces(Constant.Controller.SocialNetworkAccount.Produces)]
	[Route(Constant.Controller.SocialNetworkAccount.Route)]
	[Authorize]
	public class SocialNetworkAccountController : Controller
	{
		private readonly InstaStatisticsContext _db;
		private readonly IMapper _mapper;
		private readonly UserManager<ApplicationUser> _userManager;

		public SocialNetworkAccountController(
			InstaStatisticsContext db,
			IMapper mapper,
			UserManager<ApplicationUser> userManager)
		{
			_db = db;
			_mapper = mapper;
			_userManager = userManager;
		}

		[HttpGet]
		public async Task<IActionResult> Get()
		{
			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				return BadRequest();
			}

			List<SocialNetworkAccount> socialNetworksAccounts = _db.SocialNetworkAccounts.Where(account => account.UserId == user.Id).ToList();

			return Ok(_mapper.Map<List<SocialNetworkAccount>, List<SocialNetworkAccountDto>>(socialNetworksAccounts));
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] SocialNetworkAccountDto model)
		{
			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				return BadRequest();
			}

			SocialNetworkAccount account = _mapper.Map<SocialNetworkAccountDto, SocialNetworkAccount>(model);
			account.UserId = user.Id;

			if (account.SocialNetworkAccountId == 0)
			{
				bool isLoginExist = _db.SocialNetworkAccounts.Any(m => m.Login == account.Login);
				if (isLoginExist)
				{
					return BadRequest();
				}

				using (Task<EntityEntry<SocialNetworkAccount>> result = _db.SocialNetworkAccounts.AddAsync(account))
				{
					if (!result.IsCompletedSuccessfully)
					{
						return BadRequest();
					}
				}
			}
			else
			{
				_db.SocialNetworkAccounts.Update(account);
			}

			_db.SaveChanges();

			return Ok();
		}


		[HttpDelete]
		[Route(Constant.Controller.SocialNetworkAccount.DeleteAsync.Route)]
		public async Task<IActionResult> DeleteAsync(int id)
		{
			// TODO: Do we need this block?
			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				return BadRequest();
			}
			SocialNetworkAccount account = _db.SocialNetworkAccounts.FirstOrDefault(m => m.SocialNetworkAccountId == id);
			if (account == null)
			{
				return NotFound();
			}
			_db.Entry(account).State = EntityState.Deleted;
			_db.SaveChanges();

			return Ok();
		}
	}
}