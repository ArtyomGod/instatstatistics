﻿using System;
using System.Runtime.Serialization;
using ConstantLibrary;

namespace InstaStatistics.Dto
{
	[DataContract]
	public class StatisticsDto
	{
		[DataMember(Name = Constant.Dto.Statistics.MetricName.Name)]
		public string MetricName { get; set; }

		[DataMember(Name = Constant.Dto.Statistics.DateTimes.Name)]
		public DateTime[] DateTimes { get; set; }

		[DataMember(Name = Constant.Dto.Statistics.Values.Name)]
		public int[] Values { get; set; }
	}
}
