﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using ConstantLibrary;

namespace InstaStatistics.Dto
{
	[DataContract]
	public class ApplicationUserDto
	{
		[DataMember(Name = "Id")]
		public int Id { get; set; }

		[Required]
		[StringLength(Constant.Dto.Shared.Email.MaxLength, MinimumLength = Constant.Dto.Shared.Email.MinLength)]
		[DataMember(Name = Constant.Dto.Shared.Email.Name)]
		public string Email { get; set; }

		[Required]
		[StringLength(Constant.Dto.Shared.Password.MaxLength, MinimumLength = Constant.Dto.Shared.Password.MinLength)]
		[DataMember(Name = Constant.Dto.Shared.Password.Name)]
		public string Password { get; set; }

		[StringLength(32)]
		[DataMember(Name = "UserName")]
		public string Username { get; set; }
	}
}
