﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using ConstantLibrary;

namespace InstaStatistics.Dto
{
	[DataContract]
	public class SocialNetworkAccountDto
	{
		[DataMember(Name = Constant.Dto.SocialNetworkAccount.SocialNetworkAccountId.Name)]
		[DefaultValue(Constant.Dto.SocialNetworkAccount.SocialNetworkAccountId.Default)]
		public int SocialNetworkAccountId { get; set; }

		[DataMember(Name = Constant.Dto.SocialNetworkAccount.AccessToken.Name)]
		public string AccessToken { get; set; }

		[DataMember(Name = Constant.Dto.SocialNetworkAccount.TokenLifetime.Name)]
		public long? TokenLifetime { get; set; }

		[DataMember(Name = Constant.Dto.SocialNetworkAccount.Login.Name)]
		public string Login { get; set; }

		[DataMember(Name = Constant.Dto.Shared.Password.Name)]
		[StringLength(Constant.Dto.Shared.Password.MaxLength, MinimumLength = Constant.Dto.Shared.Password.MinLength)]
		public string Password { get; set; }
	}
}
