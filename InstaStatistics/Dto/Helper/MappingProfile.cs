﻿using System;
using AutoMapper;
using EntityFramework;
using InstaStatistics.Dto;

namespace InstaStatistics.Helper
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<ApplicationUser, ApplicationUserDto>();
			CreateMap<ApplicationUserDto, ApplicationUser>();
			CreateMap<SocialNetworkAccount, SocialNetworkAccountDto>();
			CreateMap<SocialNetworkAccountDto, SocialNetworkAccount>();
		}
	}
}