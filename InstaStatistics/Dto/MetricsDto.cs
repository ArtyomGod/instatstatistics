﻿using System.Runtime.Serialization;
using ConstantLibrary;

namespace InstaStatistics.Dto
{
	[DataContract]
	public class MetricsDto
	{
		[DataMember(Name = Constant.Dto.MetricsDto.Metric.Name)]
		public StatisticsDto[] Metric { get; set; }
	}
}
