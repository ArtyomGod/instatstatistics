﻿namespace ConstantLibrary
{
	public enum ProxyType
	{
		Https = 0,
		Socks5
	}

	public enum PromotionTaskType
	{//Добавлять новые типы всегда в конец !!!
		 SubscribeByTagInstagram = 0,
		 UnsubscribeInstagram,
		 SubscribeByCompetitor,
		 LikeFollowers,
		 LikeByTags,
		 SubscribeByLocationsInstagram,
		 LikeByLocationsInstagram
	};
	
	public static class Constant
	{
		public static class ProxyHelper
		{
			public const string AddresToCheckProxyResponseStatus = "https://www.google.com";
		}

		public static class SocialNetworkConstraints
		{
			public static int MaxAmmountToLikePerDayInst = 950; //1000
			public static int MaxAmmountToSubscribePerDayInst = 950; //1000;  per hour 200
			public static int MaxAmmountToUnsubscribePerDayInst = 950; //1000;  per hour 200
		}

		public static class InstagramApiHelper
		{
			public const int DefaultPageToLoadForPagination = 1;
			public const string StateFileName = "inst_";
			public const string FileType = "bin";
			public const char Dot = '.';
			public const string StateFilePath = "./StateInstApi/";
		}

		public static class ConnectionString
		{
			public const string Instastatistics = "server=localhost;port=3306;database=instastatistics_main;user=root;password=;SslMode=none";
			public const string Identity = "server=localhost;port=3306;database=instastatistics_secure;user=root;password=;SslMode=none";
		}

		public static class Jwt
		{
			public const string Issuer = "http://yourdomain.com";
			public const string Key = "Mc8UGtw1bLZ01pL9OhMq-DMsFGuoXfL8ZkV4wfMzf-instastatistics-273naXajepYWPzEs13ty-td4mrb5PoZmfJtzKrYeK";
			public const int ExpireDays = 30;
		}

		public static class Role
		{
			public const string Admin = "admin";
			public const string CommonUser = "common-user";
		}

		public static class SocialNetwork
		{
			public static class Instagram
			{
				public const int Id = 0;
			}

			public static class Vk
			{
				public const int Id = 1;
			}
		}

		public static class MetricHelperLibrary
		{
			public static int MetricDefaultValue = 0;
			public static class JobName
			{
				public static string FlushMetricInst = "flush-metric-inst";
				public static string ZeroingSubscribeMetricInstagram = "zeroing-subscribe-metric-instagram";
				public static string ZeroingLikeMetricInstagram = "zeroing-like-metric-instagram";
			}
		}

		public static class EmailSender
		{
			public const string Smtp = "smtp.gmail.com";
			public const int SmtpPort = 587;
			public const bool UseSsl = false;
			public const string FromAddress = "instastatistics.solutions@gmail.com";
			public const string FromName = "Администрация сайта";
			public const string FromPassword = "helloBoy";

			public static class EmailUpdating
			{
				public const string Message = @"Thank you for updating your email. Please confirm the email by clicking this link:
				<br><a href='{0}'>Confirm new email</a>";
			}

			public static class EmailConfirmation
			{
				public const string Subject = "Подвтерждение пароля";
			}
		}

		public static class Redis
		{
			public const string ConnectionString = "localhost:6379";
			public const char Delimiter = '|';

			public static class Prefix
			{
				
			}
		}

		public static class RabbitMq
		{
			public const string ConnectionString = "localhost";

			public static class ExchangeName
			{
				public const string Email = "email";
			}

			public static class QueueName
			{
				public const string Log = "email";
			}

			public const char Delimiter = '|';
		}

		public static class Controller
		{
			public static class Auth
			{
				public const string Route = "auth";

				public static class SignIn
				{
					public const string Route = "sign-in";
				}

				public static class SignUp
				{
					public const string Route = "sign-up";
				}

				public static class Confirmation
				{
					public const string Route = "confirmation";
				}

				public static class SendConfirmation
				{
					public const string Route = "send-confirmation";
				}
			}

			public static class ProxiesManagerController
			{
				public const string Produces = "application/json";
				public const string Route = "proxy-manager";

				public static class SignIn
				{
					public const string Route = "sign-in";
				}
			}

			public static class InstagramPromotion
			{
				public const string Produces = "application/json";
				public const string Route = "instagram-promotion";

				public static class Post
				{
					public const string Route = "by-tags";
				}
				public static class SubscribeByCompetitor
				{
					public const string Route = "by-competitors";
				}
				public static class SubscribeByLocataions
				{
					public const string Route = "by-locations";
				}
				public static class LikeByLocataions
				{
					public const string Route = "like-by-locations";
				}
				public static class LikeFollowers
				{
					public const string Route = "like-followers";
				}
				public static class LikeByTag
				{
					public const string Route = "like-by-tag";
				}

				public static class Put
				{
					public const string Route = "{id}";
				}

				public static class Delete
				{
					public const string Route = "{id}";
				}
			}

			public static class SocialNetworkAccount
			{
				public const string Produces = "application/json";
				public const string Route = "accounts";

				public static class DeleteAsync
				{
					public const string Route = "{id}";
				}
			}

			public static class Statistics		
			{
				public const string Produces = "application/json";
				public const string Route = "statistics";
			}

			public static class SocialPromotion
			{
				public const string Produces = "application/json";
				public const string Route = "promotion";
				public static class GetAllUserPromotionTasks
				{
					public const string Route = "get-tasks";
				}

				public static class GetUserPromotionTasksBySocialNetworkId
				{
					public const string Route = "get-task-by-social-network-id/{socialNetworkId:int}";
				}
				public static class GetPromotionTask
				{
					public const string Route = "get-task/{promotionTaskId:int}";
				}

				public static class ChangeRecurringPeriodOfTask
				{
					public const string Route = "change-recurring-period-task";
				}

				public static class Put
				{
					public const string Route = "{id}";
				}

				public static class Delete
				{
					public const string Route = "{taskId:int}";
				}
			}

			public static class UserServices
			{
				public const string Produces = "application/json";
				public const string Route = "services";

				public static class GetUsersServices
				{
					public const string Route = "get-all";
				}

				public static class GetUserServices
				{
					public const string Route = "my-services";
				}
				public static class GetUserServicesById
				{
					public const string Route = "{userId:int}";
				}

				public static class AddUserService
				{
					public const string Route = "{serviceId:int}";
				}

				public static class DeleteUserService
				{
					public const string Route = "{serviceId:int}";
				}
			}

			public static class UserSettings
			{
				public const string Produces = "application/json";
				public const string Route = "user-settings";
			}

			public static class Values
			{
				public const string Route = "values";

				public static class Get
				{
					public const string Route = "{id}";
				}

				public static class Put
				{
					public const string Route = "{id}";
				}

				public static class Delete
				{
					public const string Route = "{id}";
				}
			}
			public static class MetricHelperController
			{
				public const string Route = "metric-helper";

				public const string Produces = "application/json";
				public static class RunLikeMetricFlush
				{
					public const string Route = "run-flush-inst";
				}

				public static class AddJobMetricFlush
				{
					public const string Route = "add-job-flush-inst";
				}

				public static class GetLikeMetricPerDay
				{
					public const string Route = "get-like-metric/{id:int}";
				}
				public static class GetSubMetricPerDay
				{
					public const string Route = "get-sub-metric/{id:int}";
				}
				public static class GetUnsubMetricPerDay
				{
					public const string Route = "get-unsub-metric/{id:int}";
				}
			}
			
		}

		public static class Dto
		{
			public static class Shared
			{
				public static class Email
				{
					public const string Name = "email";
					public const int MinLength = 6;
					public const int MaxLength = 256;
				}

				public static class Password
				{
					public const string Name = "password";
					public const int MinLength = 6;
					public const int MaxLength = 32;
				}

				public static class Username
				{
					public const string Name = "username";
					public const int MinLength = 2;
					public const int MaxLength = 32;
				}
			}

			public static class SocialNetworkAccount
			{
				public static class SocialNetworkAccountId
				{
					public const string Name = "socialNetworkAccountId";
					public const int Default = 0;
				}

				public static class SocialNetworkId
				{
					public const string Name = "socialNetworkId";
				}

				public static class AccessToken
				{
					public const string Name = "accessToken";
				}

				public static class TokenLifetime
				{
					public const string Name = "tokenLifetime";
				}

				public static class Login
				{
					public const string Name = "login";
				}

				public static class Proxy
				{
					public const string Name = "proxy";
				}
			}

			public static class Statistics
			{
				public static class MetricName
				{
					public const string Name = "metric_name";
				}

				public static class DateTimes
				{
					public const string Name = "dateTimes";
				}

				public static class Values
				{
					public const string Name = "values";
				}		
			}

			public static class MetricsDto
			{
				public static class Metric
				{
					public const string Name = "metric";
				}
			}

			public static class UserSettings
			{
				public static class NewPassword
				{
					public const string Name = "newPassword";
				}

				public static class CurrentPassword
				{
					public const string Name = "currentPassword";
				}

				public static class EmailConfirmed
				{
					public const string Name = "emailConfirmed";
				}
			}
		}
	}
}
