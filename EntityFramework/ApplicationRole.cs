﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace EntityFramework
{
	[Table("AspNetRoles")]
	public class ApplicationRole : IdentityRole<int>
	{

	}
}
