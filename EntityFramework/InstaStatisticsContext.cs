﻿using Microsoft.EntityFrameworkCore;

namespace EntityFramework
{
	public class InstaStatisticsContext : DbContext
	{
		public InstaStatisticsContext (DbContextOptions<InstaStatisticsContext > options) : base(options)
		{

		}

		public InstaStatisticsContext ()
		{
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseMySql(ConstantLibrary.Constant.ConnectionString.Instastatistics);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Statistics>().HasKey(u => new { u.Date, u.AccountId });
		}
		
		public DbSet<SocialNetworkAccount> SocialNetworkAccounts { get; set; }
		public DbSet<Statistics> StatisticsSet { get; set; }
	}
}
