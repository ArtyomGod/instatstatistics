using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace EntityFramework
{
    public class IdentityContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
	{
		public IdentityContext(DbContextOptions<IdentityContext> options) : base(options)
		{

		}
    }
}