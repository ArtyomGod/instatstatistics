﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework
{
	[Table("statisctics")]
	public class Statistics
	{
		[Column("id_social_network_account")]
		public int AccountId { get; set; }
		
		[Column("date")]
		public DateTime Date { get; set; }

		[Column("likes")]
		public int Likes { get; set; }

		[Column("views")]
		public int Views { get; set; }

		[Column("comments")]
		public int Comments { get; set; }
	}
}
