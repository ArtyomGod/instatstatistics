-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Дек 24 2018 г., 00:51
-- Версия сервера: 5.6.37
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `instastatistics_main`
--

-- --------------------------------------------------------

--
-- Структура таблицы `social_network_account`
--

CREATE TABLE `social_network_account` (
  `id_social_network_account` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `token` varchar(256) NOT NULL,
  `token_lifetime` date DEFAULT NULL,
  `login` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `social_network_account`
--

INSERT INTO `social_network_account` (`id_social_network_account`, `id_user`, `token`, `token_lifetime`, `login`, `password`) VALUES
(1, 9, '', NULL, 'naturemod_on', 'xgJAjkCW56');

-- --------------------------------------------------------

--
-- Структура таблицы `statisctics`
--

CREATE TABLE `statisctics` (
  `id_social_network_account` int(11) NOT NULL,
  `date` date NOT NULL,
  `likes` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `comments` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `statisctics`
--

INSERT INTO `statisctics` (`id_social_network_account`, `date`, `likes`, `views`, `comments`) VALUES
(1, '2018-12-23', 10, 30, 40),
(1, '2018-12-24', 1, 3, 5),
(1, '2018-12-25', 3, 5, 8),
(1, '2018-12-27', 2, 43, 46),
(1, '2018-12-28', 54, 32, 2),
(1, '2018-12-29', 6, 3, 0),
(1, '2018-12-30', 4, 70, 0),
(1, '2018-12-31', 3, 1, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `social_network_account`
--
ALTER TABLE `social_network_account`
  ADD PRIMARY KEY (`id_social_network_account`),
  ADD KEY `id_user` (`id_user`);

--
-- Индексы таблицы `statisctics`
--
ALTER TABLE `statisctics`
  ADD PRIMARY KEY (`id_social_network_account`,`date`),
  ADD KEY `user_id` (`id_social_network_account`,`date`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `social_network_account`
--
ALTER TABLE `social_network_account`
  MODIFY `id_social_network_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `social_network_account`
--
ALTER TABLE `social_network_account`
  ADD CONSTRAINT `social_network_account_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `instastatistics_secure`.`AspNetUsers` (`Id`);

--
-- Ограничения внешнего ключа таблицы `statisctics`
--
ALTER TABLE `statisctics`
  ADD CONSTRAINT `statisctics_ibfk_1` FOREIGN KEY (`id_social_network_account`) REFERENCES `social_network_account` (`id_social_network_account`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
