-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Дек 24 2018 г., 00:53
-- Версия сервера: 5.6.37
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `instastatistics_secure`
--

-- --------------------------------------------------------

--
-- Структура таблицы `AspNetRoleClaims`
--

CREATE TABLE `AspNetRoleClaims` (
  `Id` int(11) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  `RoleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `AspNetRoles`
--

CREATE TABLE `AspNetRoles` (
  `Id` int(11) NOT NULL,
  `ConcurrencyStamp` longtext,
  `Name` varchar(256) DEFAULT NULL,
  `NormalizedName` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AspNetRoles`
--

INSERT INTO `AspNetRoles` (`Id`, `ConcurrencyStamp`, `Name`, `NormalizedName`) VALUES
(1, 'c82eb026-f559-481b-8bbb-86fe8b0cc088', 'admin', 'ADMIN'),
(2, 'f75b47ca-94cc-48a5-abaa-5bce2d525fe4', 'common-user', 'COMMON-USER');

-- --------------------------------------------------------

--
-- Структура таблицы `AspNetUserClaims`
--

CREATE TABLE `AspNetUserClaims` (
  `Id` int(11) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `AspNetUserLogins`
--

CREATE TABLE `AspNetUserLogins` (
  `LoginProvider` varchar(127) NOT NULL,
  `ProviderKey` varchar(127) NOT NULL,
  `ProviderDisplayName` longtext,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `AspNetUserRoles`
--

CREATE TABLE `AspNetUserRoles` (
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AspNetUserRoles`
--

INSERT INTO `AspNetUserRoles` (`UserId`, `RoleId`) VALUES
(8, 2),
(9, 2),
(10, 2),
(11, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `AspNetUsers`
--

CREATE TABLE `AspNetUsers` (
  `Id` int(11) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `ConcurrencyStamp` longtext,
  `Email` varchar(256) DEFAULT NULL,
  `EmailConfirmed` bit(1) NOT NULL,
  `LockoutEnabled` bit(1) NOT NULL,
  `LockoutEnd` datetime(6) DEFAULT NULL,
  `NormalizedEmail` varchar(256) DEFAULT NULL,
  `NormalizedUserName` varchar(256) DEFAULT NULL,
  `PasswordHash` longtext,
  `PhoneNumber` longtext,
  `PhoneNumberConfirmed` bit(1) NOT NULL,
  `SecurityStamp` longtext,
  `TwoFactorEnabled` bit(1) NOT NULL,
  `UserName` varchar(256) DEFAULT NULL,
  `SurName` varchar(255) NOT NULL,
  `email_new` varchar(256) DEFAULT NULL,
  `VkLink` varchar(255) DEFAULT NULL,
  `RegistrationDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AspNetUsers`
--

INSERT INTO `AspNetUsers` (`Id`, `AccessFailedCount`, `ConcurrencyStamp`, `Email`, `EmailConfirmed`, `LockoutEnabled`, `LockoutEnd`, `NormalizedEmail`, `NormalizedUserName`, `PasswordHash`, `PhoneNumber`, `PhoneNumberConfirmed`, `SecurityStamp`, `TwoFactorEnabled`, `UserName`, `SurName`, `email_new`, `VkLink`, `RegistrationDate`) VALUES
(8, 0, '88da6ab1-84b5-4b3b-bae0-b1f75b746cfb', 'superArthurDent.42@gmail.com', b'1', b'1', NULL, 'SUPERARTHURDENT.42@GMAIL.COM', 'SUPERARTHURDENT.42@GMAIL.COM', 'AQAAAAEAACcQAAAAEEX+eWYR58PtDTpYDtf18zz48Z30b7RpY3ymBZ2WCFIqJ3fgnPjkhg1lzZpZXhNHZg==', NULL, b'0', 'a57a8270-2e43-48ef-9403-36f78b9bcdbd', b'0', 'superArthurDent.42@gmail.com', '', NULL, NULL, '0000-00-00'),
(9, 0, '6cbea5e6-74d0-43d0-8e0d-dbcea40266b2', 'superArthurD.ent42@gmail.com', b'0', b'1', NULL, 'SUPERARTHURD.ENT42@GMAIL.COM', 'SUPERARTHURD.ENT42@GMAIL.COM', 'AQAAAAEAACcQAAAAEIy13NXSnNxMUABR7ABCJp/5T63tkmAsVc578RMmTy0msCwEWuS4+j80euNfgoESag==', NULL, b'0', 'f337af49-2ebd-4eed-8b28-2b9c027af6e1', b'0', 'superArthurD.ent42@gmail.com', '', NULL, NULL, '0000-00-00'),
(10, 0, 'd86b2bce-626a-493e-b606-8e25c6d2bffc', 'relz01@ad.ru', b'0', b'1', NULL, 'RELZ01@AD.RU', 'RELZ01@AD.RU', 'AQAAAAEAACcQAAAAEGpkIBAVDm42TlNAC2hojTEgyX/xj6wcP5pfy2tORiRfudZPySVmaLZEtbQXR6bw8w==', NULL, b'0', 'e748c53d-774e-4f34-80a8-63ef7df8a10d', b'0', 'relz01@ad.ru', '', NULL, NULL, '0000-00-00'),
(11, 0, '615c7eec-68f1-4322-a184-3f6f285ec081', 'relz01', b'0', b'1', NULL, 'RELZ01', 'RELZ01', 'AQAAAAEAACcQAAAAELdaxCcLu058+6q/zlTwpkv4n4+s2EF99UTwdjVtn21o6beYpa3CFi79rEVkQTEpyA==', NULL, b'0', '183b890d-3427-4b7a-8c2d-05c95cb8289f', b'0', 'relz01', '', NULL, NULL, '0000-00-00');

-- --------------------------------------------------------

--
-- Структура таблицы `AspNetUserTokens`
--

CREATE TABLE `AspNetUserTokens` (
  `UserId` int(11) NOT NULL,
  `LoginProvider` varchar(127) NOT NULL,
  `Name` varchar(127) NOT NULL,
  `Value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `AspNetRoleClaims`
--
ALTER TABLE `AspNetRoleClaims`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AspNetRoleClaims_RoleId` (`RoleId`);

--
-- Индексы таблицы `AspNetRoles`
--
ALTER TABLE `AspNetRoles`
  ADD PRIMARY KEY (`Id`);

--
-- Индексы таблицы `AspNetUserClaims`
--
ALTER TABLE `AspNetUserClaims`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_AspNetUserClaims_AspNetUsers_UserId` (`UserId`);

--
-- Индексы таблицы `AspNetUserLogins`
--
ALTER TABLE `AspNetUserLogins`
  ADD PRIMARY KEY (`LoginProvider`,`ProviderKey`),
  ADD KEY `FK_AspNetUserLogins_AspNetUsers_UserId` (`UserId`);

--
-- Индексы таблицы `AspNetUserRoles`
--
ALTER TABLE `AspNetUserRoles`
  ADD PRIMARY KEY (`UserId`,`RoleId`),
  ADD KEY `FK_AspNetUserRoles_AspNetRoles_RoleId` (`RoleId`);

--
-- Индексы таблицы `AspNetUsers`
--
ALTER TABLE `AspNetUsers`
  ADD PRIMARY KEY (`Id`);

--
-- Индексы таблицы `AspNetUserTokens`
--
ALTER TABLE `AspNetUserTokens`
  ADD PRIMARY KEY (`UserId`,`LoginProvider`,`Name`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `AspNetRoleClaims`
--
ALTER TABLE `AspNetRoleClaims`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `AspNetRoles`
--
ALTER TABLE `AspNetRoles`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `AspNetUserClaims`
--
ALTER TABLE `AspNetUserClaims`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `AspNetUsers`
--
ALTER TABLE `AspNetUsers`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `AspNetRoleClaims`
--
ALTER TABLE `AspNetRoleClaims`
  ADD CONSTRAINT `FK_AspNetRoleClaims_AspNetRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `instastatisctis_secure`.`AspNetRoles` (`Id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `AspNetUserClaims`
--
ALTER TABLE `AspNetUserClaims`
  ADD CONSTRAINT `FK_AspNetUserClaims_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `instastatisctis_secure`.`AspNetUsers` (`Id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `AspNetUserLogins`
--
ALTER TABLE `AspNetUserLogins`
  ADD CONSTRAINT `FK_AspNetUserLogins_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `instastatisctis_secure`.`AspNetUsers` (`Id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `AspNetUserRoles`
--
ALTER TABLE `AspNetUserRoles`
  ADD CONSTRAINT `FK_AspNetUserRoles_AspNetRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `instastatisctis_secure`.`AspNetRoles` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AspNetUserRoles_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `instastatisctis_secure`.`AspNetUsers` (`Id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `AspNetUserTokens`
--
ALTER TABLE `AspNetUserTokens`
  ADD CONSTRAINT `FK_AspNetUserTokens_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `instastatisctis_secure`.`AspNetUsers` (`Id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
